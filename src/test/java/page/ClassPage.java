package page;

import java.util.Set;
import java.util.Iterator;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;



import logic.UtilityClass;

public class ClassPage extends UtilityClass {
	
	
	By linkMiSiiSelector = By.linkText("Mi Sii");
	By campoRutContribuyenteSelector = By.xpath("//input[@id='rutcntr']");
	By campoClaveTributariaSelector = By.xpath("//input[@id='clave']");
	By btnIngresarSelector = By.xpath("//button[@id='bt_ingresar']");
   
	By indicadorLoginSelector = By.xpath("//img[@src='../img/logo_ct.png']");
	
	By btnTramiteEnLineaSelector = By.xpath("/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/a[3]/li/div[1]");
	
	By barraEmisorBoletaHonorarioSelector = By.xpath("/html/body/div[1]/div[1]/div[5]/div[2]/div[2]/div[2]/div[3]/div/div[8]/div[2]/div/p[1]/a");
	By barraBoletaHonorarioSelector = By.xpath("/html/body/div[1]/div[1]/div[5]/div[2]/div[2]/div[2]/div[3]/div/div[8]/div[1]/h4/a");
	
    //By btnCierrePopUpSelector = By.xpath("//button[@class='btn btn-default']");
	By btnCierrePopUpSelector2 = By.xpath("/html/body/div[2]/div/div/div[1]/button/span");
	//By btnCierrePopUpSelector2 = By.xpath("//button[@class='close']");
    By barraConsultaBoletaHonorarioSelector = By.xpath("//a[@href='#collapseTwo']");
    By btnConsultaBoletaEmitidaSelector = By.linkText("Consultar boletas emitidas");
    By btnanioEmsionSelector = By.xpath("//option[@value='2021']");
    By btnConsultaEmisionselector = By.xpath("//input[@id='cmdconsultar124']");
    
    By btnImprimirInformeSelector = By.xpath("//input[@name='CmdImprimit']");
    
    public ClassPage(WebDriver driver) {
		super(driver);
		
	}

	public void consultaBoletaSii () throws InterruptedException {
		
			click(linkMiSiiSelector);
		   	
		
			if(isDisplayed(indicadorLoginSelector)) {
			type("12563236k", campoRutContribuyenteSelector);
			type("pirihuinqui", campoClaveTributariaSelector);
			click(btnIngresarSelector);
			Thread.sleep(2000);
			}
			click(btnTramiteEnLineaSelector);
			Thread.sleep(2000);
			click(barraBoletaHonorarioSelector);
			Thread.sleep(2000);
			click(barraEmisorBoletaHonorarioSelector);
			Thread.sleep(2000);
		
			//manejo de la ventana emergente		
			String parentWindowHandler = driver.getWindowHandle(); // Almacena tu ventana actual
			String subWindowHandler = null;
			Set<String> handles = driver.getWindowHandles(); // Obten todas las ventana abiertas
			Iterator<String> iterator = handles.iterator();
			while (iterator.hasNext()){
			    subWindowHandler = iterator.next();
			}
			// Cambiate a la ultima ventana (tu pop-up)
			driver.switchTo().window(subWindowHandler); 
			
			// pop-up				
			click(btnCierrePopUpSelector2);
			Thread.sleep(2000);
			
			// Vuelve a tu ventana principal (si lo necesitas)
			//driver.switchTo().window(parentWindowHandler);  		
			
	}

	public void solicitaInforme() throws InterruptedException {
		
		click(barraConsultaBoletaHonorarioSelector);
		Thread.sleep(2000);
			
		click(btnConsultaBoletaEmitidaSelector );
		Thread.sleep(2000);
		
		click(btnanioEmsionSelector);
		Thread.sleep(2000);
		
		click(btnConsultaEmisionselector);
		Thread.sleep(2000);
		}	
	
	public void imprimirInformeBoletas() throws InterruptedException{
		click(btnImprimirInformeSelector);
		//Thread.sleep(2000);
	 	
		
	} 
}
