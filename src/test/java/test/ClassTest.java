package test;



import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

import page.ClassPage;


public class ClassTest {
	
	private WebDriver driver;
	ClassPage classPage;

	@Before
	public void setUp() throws Exception {
		classPage = new ClassPage (driver);
		driver = classPage.chromeDriverConexion();	
		classPage.visit("https://homer.sii.cl/");
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() throws InterruptedException {
		classPage.consultaBoletaSii();
		classPage.solicitaInforme();
		classPage.imprimirInformeBoletas();
		
	}

}
